# README #

This README is for the LEDC Project : Phase 1 is going on from Sep 4 to Nov 12 2017. Phase 2 is running from Jan to Apr 2018. 

### What is this repository for? ###

* Analytics on getting tech savvy folks to come to london for work
* 0.1

### How do I get set up? ###

* You will need atom and a git bash
* Fonts and such will be given through the design team
* Database configuration - TBD
* How to run tests: on local machines and push your work 

### Who do I talk to? ###

* Daniel Wisnowski
* Dev team: Chi Mai, Alicia Leonard